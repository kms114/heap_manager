#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

bool correct_scheduling = false;

void child(void *a) {
	correct_scheduling = true;
	if (thread_wait(100, 100) == -1) {
		cout << "child doesn't like to wait" << endl;
		exit(1);
 	}
 	thread_yield();
}

void child2(void *a) {
	if(!correct_scheduling) {
		cout << "queues not FIFO" << endl;
		exit(1);
	}
	thread_yield();
}

void parent(void *a) {
  int arg;
  arg = (long int) a;

  thread_lock(1);
  if (thread_unlock(100) != -1) {
  	cout << "tried to unlock lock I didn't have" << endl;
  	exit(1);
  }
  if (thread_wait(1,1) == -1) {
  	cout << "hit error this wait case" << endl;
  	exit(1);
  }
  if (thread_wait(1,2) == -1) {
  	cout << "hit error for second cv in lock 1" << endl;
  	exit(1);
  }
  if (thread_signal(1,1) == -1) {
  	cout << "error with cv in signal" << endl;
  	exit(1);
  }
  if (thread_broadcast(1,1) == -1) {
  	cout << "problem with broadcast" << endl;
  	exit(1);
  }
  if (thread_create((thread_startfunc_t) child, (void *) 0)) {
	cout << "error in thread_create\n";
	exit(1);
  }
  if (thread_create((thread_startfunc_t) child2, (void *) 0)) {
	cout << "error in thread_create\n";
	exit(1);
  }
  thread_yield(); // goes to child, then returns to us
}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
 exit(0);
}