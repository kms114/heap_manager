#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

void t1(void *a) {
	cout << "t1 running" << endl;
	if(thread_lock(33) == -1) {
		cout << "t1 couldn't lock 33" << endl;
	}
	thread_yield();
	if(thread_unlock(33) == -1) {
		cout << "t1 couldn't unlock 33" << endl;
	}
}

void t2(void *a) {
	cout << "t2 running" << endl;
	if(thread_lock(33) == -1) {
		cout << "t2 couldn't lock 33" << endl;
	}
	cout << "back to t2" << endl;
	thread_yield();
	if(thread_unlock(33) == -1) {
		cout << "t2 couldn't unlock 33" << endl;
	}
	thread_yield();
}

void t3(void *a) {
	cout << "t3 running" << endl;
	if(thread_lock(33) == -1) {
		cout << "t3 couldn't lock 33" << endl;
	}
	if(thread_signal(33, 0) == -1) {
		cout << "t3 unable to signal" << endl;
	}
}

void t4(void *a) {
	cout << "t4 running" << endl;
	if(thread_lock(33) == -1) {
		cout << "t4 couldn't lock 33" << endl;
	}
	cout << "t4 turn" << endl;
	thread_yield();
}

void t5(void *a) {
	cout << "t5 running" << endl;
	if(thread_lock(33) == -1) {
		cout << "t5 couldn't lock 33" << endl;
	}
	cout << "t5 returns" << endl;

}

void t6(void *a) {
	cout << "t6 running" << endl;
	if(thread_lock(33) == -1) {
		cout << "t6 couldn't lock 33" << endl;
	}
	cout << "t6 is back" << endl;
	thread_yield();
	if(thread_lock(13) == -1) {
		cout << "t6 couldn't lock 13" << endl;
	}
}

void parent(void *a) {
	if(thread_create( (thread_startfunc_t) t1, (void *) 100)) {
		cout << "unable to create t1" << endl;
	}
	if(thread_create( (thread_startfunc_t) t2, (void *) 100)) {
		cout << "unable to create t2" << endl;
	}
	if(thread_create( (thread_startfunc_t) t3, (void *) 100)) {
		cout << "unable to create t3" << endl;
	}
	if(thread_create( (thread_startfunc_t) t4, (void *) 100)) {
		cout << "unable to create t4" << endl;
	}
	if(thread_create( (thread_startfunc_t) t5, (void *) 100)) {
		cout << "unable to create t5" << endl;
	}
	if(thread_create( (thread_startfunc_t) t6, (void *) 100)) {
		cout << "unable to create t6" << endl;
	}

}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
 exit(0);
}