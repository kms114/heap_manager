#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

/*
* baby test
*/

int g=0;

void child(void *a) {
  thread_lock(0);
  int arg;
  arg = (long int) a;
  cout << "child, arg:" << arg << endl;  
  return; // exit with lock
}

void parent(void *a) {
	//start_preemptions(true,true,10);
  int arg;
  arg = (long int) a;
  
  cout << "parent, arg:" << arg << endl;

  if (thread_create((thread_startfunc_t) child, (void *) 0)) {
	cout << "error in thread_create\n";
	exit(1);
  }
  thread_yield(); // goes to child, then returns to us
  thread_lock(0);
  cout << "shouldn't get here" << endl;
  exit(1);
  return;
}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
}