#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

int scheduling_num = 0;

void child(void *a) {
	scheduling_num = scheduling_num +1;
	if (thread_lock(10) == -1) {
		cout << "unable to acquire lock" << endl;
		exit(1);
	}
	if (scheduling_num != 1) {
		cout << "incorrect lock queue implementation" << endl;
		exit(1);
	}
	thread_yield();
}

void child2(void *a) {
	scheduling_num = scheduling_num +2;
	if (thread_lock(10) == -1) {
		cout << "unable to acquire lock" << endl;
		exit(1);
	}
	if (scheduling_num != 3) {
		cout << "incorrect lock queue implementation" << endl;
		exit(1);
	}
	thread_yield();
}

void parent(void *a) {
	if (thread_lock(10) == -1) {
		cout << "unable to acquire lock" << endl;
		exit(1);
	}
	if (thread_create((thread_startfunc_t) child, (void *) 0)) {
		cout << "error in thread_create\n";
		exit(1);
  	}
  	if (thread_create((thread_startfunc_t) child2, (void *) 0)) {
		cout << "error in thread_create\n";
		exit(1);
  	}
	thread_yield();
}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
 exit(0);
}