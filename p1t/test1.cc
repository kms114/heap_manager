#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

/*
* Basic tests for error checking
*/

int g=0;

void loop(void *a) {
	if (thread_lock(0)) {
		cout << "thread_lock failed" << endl;
		//exit(1);
	}
	// thread_lock(1);
	// thread_unlock(1);
	if (thread_lock(0) != -1) {
		cout << "failed to check if I already have this lock\n";
		//exit(1);
	}
	if (thread_unlock(0)) {
		cout << "thread_unlock failed" << endl;
		//exit(1);
	}
	if (thread_unlock(1) != -1) {
		cout << "failed to check if I own the lock I'm unlocking\n";
		//exit(1);
	}
	if (thread_wait(1, 0) != -1) {
		cout << "failed to check if I own the lock I'm waiting on\n";
		//exit(1);
	}
	
  char *id;
  int i;

  id = (char *) a;
  cout <<"loop called with id " << (char *) id << endl;

  for (i=0; i<5; i++, g++) {
    cout << id << ":\t" << i << "\t" << g << endl;
    if (thread_yield()) {
      cout << "thread_yield failed\n";
      //exit(1);
    }
  }
  if (g != 9 && g != 10) {
	  cout << "thread scheduling issue: g =" << g << endl;
	  //exit(1);
  }
}

void parent(void *a) {
	//start_preemptions(true, true, 28);
	if (thread_libinit((thread_startfunc_t) parent, (void *) 100) != -1) {
		cout << "failed to check for repeated libinit\n";
		//exit(1);
	}
  int arg;
  arg = (long int) a;

  cout << "parent called with arg " << arg << endl;
  if (thread_create((thread_startfunc_t) loop, (void *) "child thread")) {
    cout << "thread_create failed\n";
    //exit(1);
  }

  loop( (void *) "parent thread");
}

int main() {
	if (thread_create((thread_startfunc_t) loop, (void *) "child thread") != -1) {
		cout << "failed to check for thread lib init\n";
		//exit(1);
	}
	if (thread_yield() != -1) {
		cout << "failed to check for thread lib init\n";
		//exit(1);
	}
	if (thread_lock(0) != -1 || thread_unlock(0) != -1) {
		cout << "failed to check for thread lib init\n";
		//exit(1);
	}
	if (thread_wait(0,0) != -1 || thread_signal(0,0) != -1 || thread_broadcast(0,0) != -1) {
		cout << "failed to check for thread lib init\n";
		//exit(1);
	}
	
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
}