#include <stdlib.h> 
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
#include <string.h>
#include "thread.h"
#include <vector>
using namespace std;

typedef struct sandwichOrder {
	int cashier_id;
	int sandwich_num;
} Order;

typedef struct orderBoard{
	vector<Order> orders;
	size_t size;
}Board;

Board board;
char* *fileArray;
int max_Size;
int active_threads;

int hasRoom;
int isFull;
int noThreads;
int lockBoard;
int lockConsole;

int mainThread(void* array);
int cashier(void* cashier_id);
int sandwichMaker(void* noArgs);
int printBoard();

int main(int argc, char * argv[]){
	max_Size = atoi(argv[1]);
	//fileArray holds # files at idx 0, then file names
	fileArray = new char*[argc-1];
	char* c = (char*)malloc(sizeof(char));
	sprintf(c, "%d", argc-2);
	fileArray[0] = c;
	for(int i = 2; i < argc; i++){
		fileArray[i-1] = argv[i];
	}
	board.size = 0;
	hasRoom = 1000;
	isFull = 1001;
	noThreads = 1002;
	lockBoard = 0;
	lockConsole = 1;
	thread_libinit((thread_startfunc_t) mainThread, (void*)fileArray);
	return 0;
}

int mainThread(void* array){
	start_preemptions(true, true, 57);
	char* *fileArray = (char**)array;
	int files = atoi(fileArray[0]);
	
	thread_lock(lockBoard);
	active_threads = 0;
	
	int* cashierArray[files];
	
	for(int i = 1; i <= files; i++){ // for each file
		int* arg = (int*)malloc(sizeof(*arg));
		*arg = i-1;
		cashierArray[i-1] = arg;
		thread_create((thread_startfunc_t) cashier, (void*)arg);
		//cout << fileArray[i] <<  endl;
		active_threads += 1;
	}

	thread_create((thread_startfunc_t) sandwichMaker, (void*)NULL);
	
	while (active_threads != 0) {
		thread_wait(lockBoard, noThreads);
	}

	// deallocate
	for (int i=0; i<files; i++) {
		free (cashierArray[i]);
	}
	free(fileArray[0]);
	thread_unlock(lockBoard);
	return 0;
}

int cashier(void* cashier_id_ptr) {
	// already incremented active_threads in mainThread
	thread_lock(lockBoard);
	int cashier_ID = *((int*) cashier_id_ptr); // this is a cv
	string line;
	ifstream currFile ((char*) fileArray[cashier_ID+1]);
	if (currFile.is_open()) {
		while (getline (currFile, line)) { //while there are still sandwiches in the file
			// for debugging: print the sandwich number
			// while(board.size >= max_Size){
			// 	 thread_wait(lockBoard, hasRoom);
			// }
			int sandwich = atoi(line.c_str());
			Order newOrder;
			newOrder.cashier_id = cashier_ID;
			newOrder.sandwich_num = sandwich;

			while (board.size >= max_Size){
				thread_wait(lockBoard, hasRoom);
			}

			board.orders.push_back(newOrder);
			board.size++;
			
			thread_lock(lockConsole);
//			printBoard();
			cout << "POSTED: cashier " << cashier_ID << " sandwich " << sandwich << endl; 
			thread_unlock(lockConsole);


			if(board.size == max_Size){
				thread_signal(lockBoard, isFull);
			}

			thread_wait(lockBoard, cashier_ID);
		}

		currFile.close();
	} else {
		cout << "Unable to open file";
		return -1;
	}
	// should I lock here?
	active_threads -= 1;
	
	max_Size = min(max_Size, active_threads);
	thread_lock(lockConsole);
//	cout << "max_Size: " << max_Size << endl;
	thread_unlock(lockConsole);
	if (active_threads > 0){
		thread_signal(lockBoard, isFull);
	}
	else{
		thread_signal(lockBoard, noThreads);
	}

	thread_unlock(lockBoard);
	thread_lock(lockConsole);
//	cout << "EXIT: " << cashier_ID << endl;
	thread_unlock(lockConsole);

	return 0;
}

int sandwichMaker(void* noArgs){
	thread_lock(lockBoard);
	int prevSand = 0;
	while(max_Size > 0) {
		while(board.size < max_Size){
			//thread_lock(lockConsole);
			//cout << "WAITING:" << "Board Size: " << board.size << " Max Size: " << max_Size << endl;
			//thread_unlock(lockConsole);
			thread_wait(lockBoard, isFull);
		}

		int indexClosest;
		int minDistance = 1001;

		for (int i = 0; i < board.size; i++){
			int difference = abs(board.orders[i].sandwich_num - prevSand);
			//cout << "In min loop" << endl;
			if (minDistance > difference){
				//cout << "index: " << i << "difference: " << difference << "minDistance: " << minDistance << endl;
				minDistance = difference;
				indexClosest = i;
			}
		}

		int chosenSandNum = board.orders[indexClosest].sandwich_num;
		int chosenCashierID = board.orders[indexClosest].cashier_id;

		prevSand = chosenSandNum;
		board.orders.erase(board.orders.begin() + indexClosest);
		board.size--;

		thread_lock(lockConsole);
//		printBoard();
		cout << "READY: cashier " << chosenCashierID << " sandwich " << chosenSandNum << endl;
		thread_unlock(lockConsole);

		thread_signal(lockBoard, hasRoom);
		thread_signal(lockBoard, chosenCashierID);
		
	}

	thread_unlock(lockBoard);
	return 0;
}

int printBoard(){
	cout << "ORDER BOARD: { ";
	for (int i = 0; i < board.orders.size(); i++){
		cout << board.orders[i].sandwich_num << ", ";
	}
	cout << "}" << endl;
}



