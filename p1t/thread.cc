#include <stdlib.h> 
#include <iostream> 
#include "thread.h"
#include "interrupt.h"
#include <ucontext.h>
#include <assert.h>
#include <vector>
#include <map>
#include <new>
#include <errno.h>
using namespace std;

/*
* we pass/store pointers to Thread structs that contain ucontext structs
* this way, we never copy the original ucontext struct
*/

#define STACK_SIZE 262144

typedef struct lock{
	int lock_id;
	ucontext_t* lockHolder;
	vector<ucontext_t*> lockQueue;
	map<int, vector<ucontext_t*> > cvMap;
} lock_t;

ucontext_t* runningThread;
vector<ucontext_t*> readyQueue;
map<int, lock_t> lockMap;
vector<ucontext_t*> deadThreads;

bool initiated = false;

int thread_libinit(thread_startfunc_t func, void *arg);
int thread_create(thread_startfunc_t func, void *arg);
int thread_yield();
int thread_lock(unsigned int lock);
int thread_unlock(unsigned int lock);
int thread_wait(unsigned int lock, unsigned int cond);
int thread_signal(unsigned int lock, unsigned int cond);
int thread_broadcast(unsigned int lock, unsigned int cond);
int stub(thread_startfunc_t func, void *arg);
int cleanUp();
int contextSwitch();

int thread_libinit(thread_startfunc_t func, void *arg) {
	interrupt_disable();
	if (func == NULL) {
		interrupt_enable();
		return -1;
	}
	if (initiated){
		interrupt_enable();
		return -1;
	}
	initiated = true;
	char* stack;
	try {
		runningThread = new ucontext_t;
		assert(getcontext(runningThread) != -1);
		stack = new char[STACK_SIZE];
	} catch (bad_alloc& e){
		return -1;
	}
	runningThread->uc_stack.ss_sp = stack;
	//cout << "init sp: " << runningThread->uc_stack.ss_sp << endl;
	runningThread->uc_stack.ss_size = STACK_SIZE;
	runningThread->uc_stack.ss_flags = 0;
	runningThread->uc_link = NULL;
	errno = 0;
	makecontext(runningThread, (void (*)()) stub, 2, func, arg);
	assert(errno == 0);
	//interrupt_disable();
	//cout << "ucontext: " << runningThread << endl;
	assert(setcontext(runningThread) != -1);
}

int thread_create(thread_startfunc_t func, void *arg) {
	interrupt_disable();
	if (func == NULL) {
		interrupt_enable();
		return -1;
	}
	if (!initiated){
		interrupt_enable();
		return -1;
	}
	//cout << "thread create " << func << " arg: " << arg << endl;
	char* stack;
	ucontext_t* nextThread;
	try {
		nextThread = new ucontext_t;
		assert(getcontext(nextThread) != -1);
		stack = new char[STACK_SIZE];
	} catch (bad_alloc& e){
		interrupt_enable();
		return -1;
	}
	nextThread->uc_stack.ss_sp = stack;
	//cout << "thread create sp: " << nextThread->uc_stack.ss_sp << endl;
	nextThread->uc_stack.ss_size = STACK_SIZE;
	nextThread->uc_stack.ss_flags = 0;
	nextThread->uc_link = NULL;
	errno = 0;
	makecontext(nextThread, (void (*)()) stub, 2, func, arg);
	assert(errno == 0);
	readyQueue.push_back(nextThread);
	interrupt_enable();
	return 0;
}

int thread_yield() { // not sure if we keep "void" in function signature?
	interrupt_disable();
	if (!initiated){
		interrupt_enable();
		return -1;
	}
	//cout << "yield\n";
	
	readyQueue.push_back(runningThread);
	contextSwitch();
	interrupt_enable();
	return 0;
}

int thread_lock(unsigned int lock){
	interrupt_disable();
	if (!initiated){
		interrupt_enable();
		return -1;
	}
	//cout << "thread_lock " << lock << endl;
	if (lockMap.find(lock) == lockMap.end()){
		lock_t newLock;
		newLock.lock_id = lock;
		newLock.lockHolder = NULL;
		vector<ucontext_t*> lq;
		map<int, vector<ucontext_t*> > newcvMap;
		newLock.lockQueue = lq;
		newLock.cvMap = newcvMap;
		lockMap[lock] = newLock;
	}
	lock_t* lockObj = &lockMap[lock];
	
	if (lockObj->lockHolder == runningThread){
		interrupt_enable();
		return -1;
	}
	if(lockObj->lockHolder == NULL){
		lockObj->lockHolder = runningThread;
		interrupt_enable();
	}
	else{
		lockObj->lockQueue.push_back(runningThread);
		if (readyQueue.size() != 0){
			contextSwitch();
			interrupt_enable(); // anna_added
		}
		else{
			cleanUp();
		}
	}
	return 0;
}

int thread_unlock(unsigned int lock){
	interrupt_disable();
	if (!initiated){
		interrupt_enable();
		return -1;
	}
	//cout << "thread unlock " << lock << endl;
	if (lockMap.find(lock) == lockMap.end()){
		interrupt_enable();
		return -1;
	}
	lock_t* lockObj = &lockMap[lock];
	if (lockObj->lockHolder != runningThread){
		interrupt_enable();
		return -1;
	}
	lockObj->lockHolder = NULL;
	if (lockObj->lockQueue.size() != 0){
		readyQueue.push_back(lockObj->lockQueue[0]);
		lockObj->lockHolder = lockObj->lockQueue[0];
		lockObj->lockQueue.erase(lockObj->lockQueue.begin());
	}
	interrupt_enable();
	return 0;
}

int thread_unlock_func(unsigned int lock) {
	if (lockMap.find(lock) == lockMap.end()){
		return -1;
	}
	lock_t* lockObj = &lockMap[lock];
	if (lockObj->lockHolder != runningThread){
		return -1;
	}
	lockObj->lockHolder = NULL;
	if (lockObj->lockQueue.size() != 0){
		readyQueue.push_back(lockObj->lockQueue[0]);
		lockObj->lockHolder = lockObj->lockQueue[0];
		lockObj->lockQueue.erase(lockObj->lockQueue.begin());
	}
	return 0;
}
int thread_lock_func(unsigned int lock) {
	if (lockMap.find(lock) == lockMap.end()){
		lock_t newLock;
		newLock.lock_id = lock;
		newLock.lockHolder = NULL;
		vector<ucontext_t*> lq;
		map<int, vector<ucontext_t*> > newcvMap;
		newLock.lockQueue = lq;
		newLock.cvMap = newcvMap;
		lockMap[lock] = newLock;
	}
	lock_t* lockObj = &lockMap[lock];
	
	if (lockObj->lockHolder == runningThread){
		return -1;
	}
	if(lockObj->lockHolder == NULL){
		lockObj->lockHolder = runningThread;
	}
	else{
		lockObj->lockQueue.push_back(runningThread);
		if (readyQueue.size() != 0){
			contextSwitch();
		}
		else{
			cleanUp();
		}
	}
	return 0;
}

int thread_wait(unsigned int lock, unsigned int cond){
	interrupt_disable();
	if (!initiated){
		interrupt_enable();
		return -1;
	}
	//cout << "thread wait " << lock << " cond: " << cond << endl;
	if (thread_unlock_func(lock) == -1){
		interrupt_enable();
		return -1;
	}
	
	lock_t* lockObj = &lockMap[lock];
	//assert(lockObj);
	if(lockObj->cvMap.find(cond) == lockObj->cvMap.end()){
		vector<ucontext_t*> cvQ;
		lockObj->cvMap[cond] = cvQ;
	}
	lockObj->cvMap[cond].push_back(runningThread);
	if (readyQueue.size() != 0){
		contextSwitch();
		thread_lock_func(lock);
		interrupt_enable(); // anna_added
		return 0;
	}
	else{
		cleanUp();
	}
}

int thread_signal(unsigned int lock, unsigned int cond){
	interrupt_disable();
	if (!initiated){
		interrupt_enable();
		return -1;
	}
	//cout << "thread signal " << lock << " cond: " << cond << endl;
	if (lockMap.find(lock) == lockMap.end()){
	 	lock_t newLock;
	 	newLock.lock_id = lock;
	 	newLock.lockHolder = NULL;
	 	vector<ucontext_t*> lq;
	 	map<int, vector<ucontext_t*> > newcvMap;
	 	newLock.lockQueue = lq;
	 	newLock.cvMap = newcvMap;
	 	lockMap[lock] = newLock;
	 }
	lock_t* lockObj = &lockMap[lock];
	if(lockObj->cvMap.find(cond) == lockObj->cvMap.end()){
		vector<ucontext_t*> cvQ;
		lockObj->cvMap[cond] = cvQ;
	}
	else{
		if (lockObj->cvMap[cond].size() > 0) {
			readyQueue.push_back(lockObj->cvMap[cond][0]);
			
			lockObj->cvMap[cond].erase(lockObj->cvMap[cond].begin());
		}
		
	}
	interrupt_enable();
	return 0;
}

int thread_broadcast(unsigned int lock, unsigned int cond){
	interrupt_disable();
	if (!initiated){
		interrupt_enable();
		return -1;
	}
	//cout << "thread broadcast " << lock << " cond: " << cond << endl;
	if (lockMap.find(lock) == lockMap.end()){
		lock_t newLock;
		newLock.lock_id = lock;
		newLock.lockHolder = NULL;
		vector<ucontext_t*> lq;
		map<int, vector<ucontext_t*> > newcvMap;
		newLock.lockQueue = lq;
		newLock.cvMap = newcvMap;
		lockMap[lock] = newLock;
	}
	lock_t* lockObj = &lockMap[lock];
	if(lockObj->cvMap.find(cond) == lockObj->cvMap.end()){
		vector<ucontext_t*> cvQ;
		lockObj->cvMap[cond] = cvQ;
	}
	for(int i = 0; i < lockObj->cvMap[cond].size(); i++){
		readyQueue.push_back(lockObj->cvMap[cond][0]);
		lockObj->cvMap[cond].erase(lockObj->cvMap[cond].begin());
	}
	interrupt_enable();
	return 0;
}

int stub(thread_startfunc_t func, void *arg){
	interrupt_enable();
	func(arg);
	interrupt_disable();
	cleanUp();
}

int cleanUp(){
	// clean up everything in trash
	for (int i=0; i<deadThreads.size(); i++) {
		ucontext_t* toDelete = deadThreads[i];
		delete((char*) toDelete->uc_stack.ss_sp);
		delete toDelete;
	}
	deadThreads.clear();
	// add currently running to trash
	deadThreads.push_back(runningThread);
	
	//cout << "cleaning " << runningThread->uc_stack.ss_sp << endl;
	if(readyQueue.size()!=0){
		contextSwitch(); 
		interrupt_enable; //anna_added
	}
	else{
		//cout << "final cleanup\n";
		//map<int, lock_t>::iterator i;
		//for (i = lockMap.begin(); i != lockMap.end(); i++){
			// lock_t lock = i->second; 
			// // i->second = value
			// // i->first = key
			// for (int k=0; k< lock.lockQueue.size(); k++) {
				// cout << "deleting sp: " << lock.lockQueue[k]->uc_stack.ss_sp << endl;
				// cout << "ucontext ptr: " << lock.lockQueue[k] << endl;
				// delete[] (char*)lock.lockQueue[k]->uc_stack.ss_sp;
				// delete(lock.lockQueue[k]);
			// }
			// // is the below right??
			// map<int, vector<ucontext_t*> >::iterator j;
			// for (j = lock.cvMap.begin(); j != lock.cvMap.end(); j++) {
				// vector<ucontext_t*> cvQueue = j->second;;
				// for (int l=0; l< cvQueue.size(); l++) {
					// cout << "deleting sp: " << cvQueue[l]->uc_stack.ss_sp << endl;
					// cout << "ucontext ptr: " << cvQueue[l] << endl;
					// delete[] (char*)cvQueue[l]->uc_stack.ss_sp;
					// delete(cvQueue[l]);
				// }
			// }
		//}
		cout << "Thread library exiting." << endl;
		//interrupt_enable();
		exit(0);
	}
}

int contextSwitch(){
	ucontext_t* nextThread = readyQueue[0];
	readyQueue.erase(readyQueue.begin());
	ucontext_t* prev = runningThread;
	runningThread = nextThread;
	//cout << "swapping: " << prev << " to " << runningThread << endl;
	assert(swapcontext(prev, runningThread) != -1);
	
}

