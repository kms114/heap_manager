#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

int g=0;

void child(void *a) {
  int id;
  id = (long int) a;
  
  thread_lock(0);
  if (id == 0) {
	thread_wait(0,0);
	cout << "shouldn't wake here\n";
  } else {
	thread_signal(1,0); // signal wrong cv - shouldn't wake other up
  }
  
}

void parent(void *a) {
  int arg;
  arg = (long int) a;
  if (arg != 100) {
	cout << "failed to pass args correctly" << endl;
	//exit(1);
  }

  thread_create((thread_startfunc_t) child, (void *) 0);
  thread_create((thread_startfunc_t) child, (void *) 1);

}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    //exit(1);
  }
}