#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

void thread4(void *a) {
	cout << "thread4 running" << endl;
	if (thread_lock(10) == -1) {
		cout << "thread4 lock error" << endl;
	}
	if(thread_broadcast(11,11) == -1) {
		cout << "thread4 broadcast error" << endl;
	}
	if(thread_wait(11,11) == -1) {
		cout << "thread4 wait error" << endl;
	}
	if(thread_lock(11) == -1) {
		cout << "thread4 lock error 11" << endl;
	}
	if(thread_wait(11,11) == -1) {
		cout << "thread4 wait error 11" << endl;
	}
	if(thread_lock(11) == -1) {
		cout << "WE HIT but couldn't lock 11" << endl;
	}
	cout << "thread4 running post wait" << endl;
	thread_yield();
}

void thread3(void *a) {
	cout << "thread3 running" << endl;
	if (thread_lock(10) == -1) {
		cout << "thread3 lock error" << endl;
	}
	if(thread_broadcast(11,11) == -1) {
		cout << "thread3 broadcast error" << endl;
	}
	if(thread_wait(11,11) == -1) {
		cout << "thread3 wait error" << endl;
	}
	if(thread_lock(11) == -1) {
		cout << "thread3 lock error 11" << endl;
	}
	if(thread_wait(11,11) == -1) {
		cout << "thread3 wait error 11" << endl;
	}
	thread_yield();
}

void thread2(void *a) {
	cout << "thread 2 running" << endl;
	if (thread_signal(10,10) == -1) {
		cout << "thread2 signal error" << endl;
	}
	if (thread_lock(10) == -1) {
		cout << "thread2 can't obtain lock 10" << endl;
	}
	if(thread_wait(10,10) == -1) {
		cout << "thread2 wait error" << endl;
	}
	cout << "POST WAIT THREAD2" << endl;
	if (thread_lock(10) == -1) {
		cout << "thread2 post wait" << endl;
	}
	if (thread_create((thread_startfunc_t) thread4, (void *) 0)) {
		cout << "error creating thread4" << endl;
	}
	cout << "thread2 running again" << endl;
	thread_yield();
}

void thread1(void *a) {
	if (thread_create((thread_startfunc_t) thread3, (void *) 0)) {
		cout << "error creating thread3" << endl;
	}
	if(thread_broadcast(10, 10) == -1) {
		cout << "broadcast fail" << endl;
	}
	if (thread_create((thread_startfunc_t) thread4, (void *) 0)) {
		cout << "error creating thread4" << endl;
	}
	if(thread_wait(10,10) == -1) {
		cout << "thread1 couldn't wait" << endl;
	}
	if (thread_unlock(200) == -1) {
		cout << "thread1 couldn't unlock 200" << endl;
	}
	if (thread_lock(10) == -1) {
		cout << "thread1 couldn't lock 10" << endl;
	}
	if(thread_wait(10, 10) == -1) {
		cout << "second wait for thread1 failed" << endl;
	}
	if(thread_unlock(34) == -1) {
		cout << "thread1 failed to unlock 34" << endl;
	}
	thread_yield();
}

void parent(void *a) {
	if (thread_create((thread_startfunc_t) thread1, (void *) 0)) {
		cout << "error in thread_create\n";
		//exit(1);
  	}
  	if (thread_create((thread_startfunc_t) thread2, (void *) 0)) {
		cout << "error in thread_create\n";
		//exit(1);
  	}
	if(thread_broadcast(10, 10) == -1) {
		cout << "broadcast fail" << endl;
	}
	if(thread_lock(10) == -1) {
		cout << "parent lock fails" << endl;
	}
	if (thread_wait(10,10) == -1) {
		cout << "parent wait fails" << endl;
	}
	cout << "parent about to create thread2" << endl;
	if (thread_create((thread_startfunc_t) thread2, (void *) 0)) {
		cout << "error in thread_create\n";
		//exit(1);
  	}
	thread_yield();
}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
 exit(0);
}