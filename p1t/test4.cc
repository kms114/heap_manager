#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

/*
* baby test
*/

int g=0;

void child(void *a) {
  int arg;
  arg = (long int) a;
  cout << "child, arg:" << arg << endl;
  
  thread_lock(0);
  thread_lock(1);
  g -= 1;
  if (g == 0) {
	  thread_signal(0,0);
  }
  thread_signal(1,0); // try signaling when there's nothing to signal
  cout << "unlocking " << arg << endl;
  thread_unlock(1);
  thread_unlock(0);
  return; // won't cleanup child but we haven't implemented wait yet
}

void parent(void *a) {
  //start_preemptions(true, true, 10);
  int arg;
  arg = (long int) a;
  
  cout << "parent, arg:" << arg << endl;
  
  thread_lock(0);
  for (int i=0; i<5; i++) {
	  if (thread_create((thread_startfunc_t) child, (void *) i)) {
		cout << "error in thread_create\n";
		exit(1);
	  }
	  g++;
  }
  
  thread_wait(0,0); // wait for all children to finish
  
  if (g != 0) {
	  cout << "exited prematurely (signal of cv 0)\n";
	  exit(1);
  }
  
  cout << "wait forever\n";
  if (thread_wait(1,0) != -1) {
	cout << "shouldn't wait on lock that you don't have" << endl;  
  }
  thread_wait(0,0);
  cout << "should have deadlock exited\n";
  exit(1);
}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
  exit(0);
}
