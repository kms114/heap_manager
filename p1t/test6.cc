#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

/*
* baby test
*/

int g=0;

void child(void *a) {
  thread_lock(0);
  int arg;
  arg = (long int) a;
  int i = 0;
  while (g > 0) {
	if (arg == 0) { //ping
	  g--;
	  i++;
	  thread_signal(0,0);
	  thread_wait(0,0);
	} else { //pong
	  g--; 
	  i++;
	  thread_signal(0,0);
	  thread_wait(0,0);
	}
  }
  cout << "arg " << arg << " g: " << g << " i: " << i << endl;
  if (g != 0 || i != 2) {
	  cout << "g or i wrong\n";
	  exit(1);
  }
  return;
}

void parent(void *a) {
	//start_preemptions(true,true,50);
  int arg;
  arg = (long int) a;
  
  if (thread_create( NULL, NULL)==0) {
	cout << "create should've returned -1\n";
	exit(1);
  }
  
  thread_create((thread_startfunc_t) child, (void *) 0);
  thread_create((thread_startfunc_t) child, (void *) 1);
  g = 5;

  return;
}

int main() {
  if (thread_libinit( NULL, NULL)==0) {
	cout << "libinit should've returned -1\n";
	exit(1);
  }
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
}