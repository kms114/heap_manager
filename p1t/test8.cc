#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>
using namespace std;

//THIS SHOULD TEST SIGNAL AND BROADCAST 
//-> DO THEY COME OFF THE RIGHT QUEUE AND IN THE RIGHT ORDER?

void thread1(void *a) {
	cout << "thread1 running" << endl;
	if (thread_lock(1) == -1) {
		cout << "thread1 had some problems locking" << endl;
	}
	if (thread_signal(1,1) == -1) {
		cout << "thread1 couldn't signal" << endl;
	}
	if (thread_wait(1,1) == -1) {
		cout << "thread wait problem" << endl;
	}
	if (thread_unlock(2) == -1) {
		cout << "unlock fail" << endl;
	}
	cout << "thread1 runningzzzzzzzz" << endl;
	thread_yield();
}

void thread2(void *a) {
	cout << "thread2 running" << endl;
	if (thread_lock(1) == -1) {
		cout << "thread2 had some problems locking" << endl;
	}
	if (thread_signal(1,1) == -1) {
		cout << "thread2 couldn't signal" << endl;
	}
	cout << "we should have signaled" << endl;
	if (thread_wait(1,1) == -1) {
		cout << "thread wait problem" << endl;
	}
	if (thread_lock(1) == -1) {
		cout << "thread2 locking probs" << endl;
	}
	cout << "thread2 runningzzzzzz" << endl;
	thread_yield();
}

void parent(void *a) {
	if (thread_lock(1) == -1) {
		cout << "parent had some problems locking" << endl;
	}
	if (thread_create((thread_startfunc_t) thread1, (void *) 0)) {
		cout << "error in thread_create\n";
		//exit(1);
  	}
  	if (thread_create((thread_startfunc_t) thread2, (void *) 0)) {
		cout << "error in thread_create\n";
		//exit(1);
  	}
	if (thread_wait(1,1) == -1) {
		cout << "thread wait problem parent" << endl;
	}
	cout << "parent running" << endl;
	//print the ready queue
	if (thread_create((thread_startfunc_t) thread1, (void *) 0)) {
		cout << "recreating thread1" << endl;
		//exit(1);
  	}
	thread_yield();
	if (thread_lock(1) == -1) {
		cout << "parent can't lock" << endl;
	}
	if (thread_broadcast(1,1) == -1) {
		cout << "error with broadcast" << endl;
	}
	cout << "parent broadcasted" << endl;
	thread_yield();
}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
 exit(0);
}