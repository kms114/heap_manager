#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

int g=0;

void child(void *a) {
  int id;
  id = (long int) a;
  double dummy_array[1000];
  for(int i = 0; i <1000; i++) {
    dummy_array[i] =i;
  }
  return;  
}

void parent(void *a) {
  int arg;
  arg = (long int) a;
  for(int i = 0; i < 20000; i++) {
	  if (thread_create((thread_startfunc_t) child, (void *) 0)!=0) {
		  break;
	  }
  }
  cout << "got it\n";
  return;

}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
}