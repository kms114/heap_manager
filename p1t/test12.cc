#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

void child1(void *a) {
	if(thread_lock(44) == -1) {
		cout << "child1 couldn't lock 44" << endl;
	}
	//thread_yield();
	if(thread_signal(44, 1) == -1) {
		cout << "child1 couldn't signal" << endl;
	}
	if(thread_broadcast(44, 1) == -1) {
		cout << "child1 couldn't broadcast" << endl;
	}
	if(thread_signal(44, 44) == -1) {
		cout << "another signal problem" << endl;
	}
	thread_yield();
	if(thread_signal(1,1) == -1) {
		cout << "second signal problem" << endl;
	}
	if(thread_unlock(44) == -1) {
		cout << "how about this" << endl;
	}
	cout << "end of child1" << endl;
}

void child(void *a) {
	cout << "child began running" << endl;
	if(thread_wait(44,1) == -1) {
		cout << "child couldn't wait on 44, 1" << endl;
	}
	if(thread_lock(44) == -1) {
		cout << "child couldn't obtain lock 44" << endl;
	}
	if(thread_lock(1) == -1) {
		cout << "child couldn't obtain lock 1" << endl;
	}
	if(thread_wait(44,1) == -1) {
		cout << "child couldn't wait2 on 44, 1" << endl;
	}
}

void parent(void *a) {
	cout << "parent" << endl;
	if(thread_lock(44) == -1) {
		cout << "Parent couldn't lock" << endl;
	}
	if(thread_create((thread_startfunc_t) child1, (void*) 100)) {
		cout << "couldn't create child1" << endl;
	}
 	if(thread_wait(44, 1) == -1) {
		cout << "Parent didn't wait on 44,1" << endl;
	}
	cout << "returned to parent" << endl;
	if (thread_create( (thread_startfunc_t) child, (void *) 100)) {
		cout << "unable to create child thread" << endl;
	}
	cout << "parent created child" << endl;
	if(thread_unlock(2) == -1) {
		cout << "returned from wait and couldn't unlock 2" << endl;
	}
}


int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
 exit(0);
}