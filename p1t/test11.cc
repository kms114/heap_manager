#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

int g=0;

void one() {
  thread_lock(0);
  cout << "one wait\n";
  thread_wait(0,0);
  cout << "one awake\n";
  thread_unlock(0);
  cout << "one done\n";
}
void two() {
  thread_lock(0);
  cout << "two yield\n";
  thread_yield();
  cout << "two returns\n";
  thread_unlock(0);
  cout << "two done\n";
}
void three() {
  thread_signal(0,0);
  cout << "three signals\n";
  thread_lock(0);
  cout << "three gets lock\n";
  thread_unlock(0);
  cout << "three done\n";
}

void parent(void *a) {
  thread_create((thread_startfunc_t) one, NULL);
  thread_create((thread_startfunc_t) two, NULL);
  thread_create((thread_startfunc_t) three, NULL);
  thread_create((thread_startfunc_t) one, NULL);
  thread_create((thread_startfunc_t) two, NULL);
  thread_create((thread_startfunc_t) three, NULL);
  cout << "here in test11\n";
  return;
}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    exit(1);
  }
}