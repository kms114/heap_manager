#include <stdlib.h>
#include <iostream>
#include "thread.h"
#include <assert.h>

using namespace std;

int g=0;

void child(void *a) {
  int id;
  id = (long int) a;
  
  thread_lock(0);
  if (id == 0) {
	thread_yield(); // it now goes to 1 - 1 can't get lock
	return;
  } else {
	cout << "shouldn't get here - 0 exited with lock\n";
	//exit(1);
  }
  
}

void parent(void *a) {
  int arg;
  arg = (long int) a;
  if (arg != 100) {
	cout << "failed to pass args correctly" << endl;
	//exit(1);
  }

  thread_create((thread_startfunc_t) child, (void *) 0);
  thread_create((thread_startfunc_t) child, (void *) 1);
  thread_create((thread_startfunc_t) child, (void *) 2);
  thread_create((thread_startfunc_t) child, (void *) 3);

}

int main() {
  if (thread_libinit( (thread_startfunc_t) parent, (void *) 100)) {
    cout << "thread_libinit failed\n";
    //exit(1);
  }
}