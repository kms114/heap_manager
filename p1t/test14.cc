#include <stdlib.h> 
#include <iostream>     // std::cout
#include <fstream>      // std::ifstream
#include <sstream>
#include <string.h>
#include "thread.h"
#include <vector>
#include <map>
#include <new>

using namespace std;

typedef struct sandwichOrder {
	int cashier_id;
	int sandwich_num;
} Order;
typedef struct orderBoard{
	vector<Order> orders;
	size_t size;
}Board;
Board board;
int fileArray[6];
int max_Size;
int active_threads;
int hasRoom;
int isFull;
int noThreads;
int lockBoard;
int lockConsole;
map<int, int*> cashierSandMap;

int mainThread(void* array);
int cashier(void* cashier_id);
int sandwichMaker(void* noArgs);
int printBoard();
int main(){
	max_Size = 3;
	//fileArray holds # files at idx 0, then file names
	//fileArray = new int[6];
	int c = 5;
	fileArray[0] = c;
	for(int i = 2; i < 7; i++){
		fileArray[i-1] = i;
	}
	board.size = 0;
	hasRoom = 1000;
	isFull = 1001;
	noThreads = 1002;
	lockBoard = 0;
	lockConsole = 1;
	//cout << "hi kayla" << endl;
	thread_libinit((thread_startfunc_t) mainThread, (void*)fileArray);
	return 0;
}
int mainThread(void* array){
	//start_preemptions(true, true, 41);
	thread_yield();
	//int* *fileArray = (int**)array;
	int files = 5;
	
	thread_lock(lockBoard);
	active_threads = 0;
	
	int* cashierArray[files];
	thread_yield();
	int cashier_0[2] = {53, 785};
	cashierSandMap[0] = cashier_0;
	int cashier_1[2] = {914, 350};
	cashierSandMap[1] = cashier_1;
	int cashier_2[2] = {827, 567};
	thread_yield();
	cashierSandMap[2] = cashier_2;
	int cashier_3[2] = {302, 230};
	cashierSandMap[3] = cashier_3;
	thread_yield();
	int cashier_4[2] = {631, 11};
	cashierSandMap[4] = cashier_4;
	
	for(int i = 1; i <= files; i++){ // for each file
		int* arg = (int*)malloc(sizeof(*arg));
		*arg = i-1;
		cashierArray[i-1] = arg;
		thread_yield();
		thread_create((thread_startfunc_t) cashier, (void*)arg);
		active_threads += 1;
		thread_yield();
	}
	thread_create((thread_startfunc_t) sandwichMaker, (void*)NULL);
	thread_yield();
	while (active_threads != 0) {
		thread_wait(lockBoard, noThreads);
	}
	// deallocate
	thread_yield();
	for (int i=0; i<files; i++) {
		free (cashierArray[i]);
		thread_yield();
	}
	//free(fileArray[0]);
	thread_yield();
	thread_unlock(lockBoard);
	return 0;
}
int cashier(void* cashier_id_ptr) {
	//cout << "hi kayla" << endl;
	thread_lock(lockBoard);
	int cashier_ID = *((int*) cashier_id_ptr); // this is a cv
	//string line;
	//ifstream currFile ((char*) fileArray[cashier_ID+1]);
	//cashierSandMap.find[cashier_ID];
	thread_yield();
	for(int i = 0; i < 2; i++) {
			int sandwich = cashierSandMap[cashier_ID][i];
			//cout << "sandwich #: " << sandwich << endl;
			Order newOrder;
			thread_yield();
			newOrder.cashier_id = cashier_ID;
			newOrder.sandwich_num = sandwich;
			while (board.size >= max_Size){
				thread_wait(lockBoard, hasRoom);
			}
			board.orders.push_back(newOrder);
			board.size++;
			thread_yield();
			thread_lock(lockConsole);
			cout << "POSTED: cashier " << cashier_ID << " sandwich " << sandwich << endl; 
			thread_unlock(lockConsole);
			if(board.size == max_Size){
				thread_signal(lockBoard, isFull);
			}
			thread_yield();
			thread_wait(lockBoard, cashier_ID);
		//currFile.close();
	}
	// should I lock here?
	active_threads -= 1;
	thread_yield();
	max_Size = min(max_Size, active_threads);
	thread_lock(lockConsole);
//	cout << "max_Size: " << max_Size << endl;
	thread_unlock(lockConsole);
	if (active_threads > 0){
		thread_yield();
		thread_signal(lockBoard, isFull);
	}
	else{
		thread_signal(lockBoard, noThreads);
	}
	thread_yield();
	thread_unlock(lockBoard);
	thread_lock(lockConsole);
//	cout << "EXIT: " << cashier_ID << endl;
	thread_unlock(lockConsole);
	return 0;
}
int sandwichMaker(void* noArgs){
	thread_lock(lockBoard);
	int prevSand = 0;
	thread_yield();
	while(max_Size > 0) {
		thread_yield();
		while(board.size < max_Size){
			thread_wait(lockBoard, isFull);
		}
		int indexClosest;
		int minDistance = 1001;
		thread_yield();
		for (int i = 0; i < board.size; i++){
			int difference = abs(board.orders[i].sandwich_num - prevSand);
			//cout << "In min loop" << endl;
			thread_yield();
			if (minDistance > difference){
				//cout << "index: " << i << "difference: " << difference << "minDistance: " << minDistance << endl;
				minDistance = difference;
				indexClosest = i;
			}
		}
		int chosenSandNum = board.orders[indexClosest].sandwich_num;
		int chosenCashierID = board.orders[indexClosest].cashier_id;
		prevSand = chosenSandNum;
		thread_yield();
		board.orders.erase(board.orders.begin() + indexClosest);
		board.size--;
		thread_lock(lockConsole);
		cout << "READY: cashier " << chosenCashierID << " sandwich " << chosenSandNum << endl;
		thread_unlock(lockConsole);
		thread_yield();
		thread_signal(lockBoard, hasRoom);
		thread_signal(lockBoard, chosenCashierID);
		
	}
	thread_yield();
	thread_unlock(lockBoard);
	return 0;
}
int printBoard(){
	cout << "ORDER BOARD: { ";
	for (int i = 0; i < board.orders.size(); i++){
		cout << board.orders[i].sandwich_num << ", ";
	}
	cout << "}" << endl;
}