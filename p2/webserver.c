#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>

/*
BuggyServer v. 2.1
Copyright (c) 2008-9, Matt Jacobson [mhj32]
All Rights Reserved.
*/

typedef unsigned char byte;

int main(int, char *[]);
char *handle(char *, int *);
int check_filename_length(byte);
void sigint(int);
void sigsegv(int);

char *port_arg;
int sock = 0;
const char *prefix = "GET /";
const char *resp_ok = "HTTP/1.1 200 OK\nServer: BuggyServer/1.0\n";
const char *resp_bad = "HTTP/1.1 404 Not Found\nServer: BuggyServer/1.0\n\n";
const char *content_html = "Content-type: text/html\n\n";
const char *content_text = "Content-type: text/plain\n\n";

const char *www_path = "./www/";

int main(int argc, char *argv[]) {
  int port;

  //port arguement error...
  if (argc < 2 || (port = atoi(argv[1])) < 1 || port > 65535) {
    printf("Usage: webserver port\n");
    exit(1);
  }

  port_arg = (char *)malloc(6*sizeof (char));
  memset(port_arg, '\0', 6);
  strncpy(port_arg, argv[1], 5);

  //sets a function to handle signal
  //SIGINT = external interrupt, usually initiated by the user;
  //SIGSEGV = invalid memory access (segmentation fault)
  signal(SIGINT, &sigint);
  signal(SIGSEGV, &sigsegv);
	
  struct sockaddr_in socket_addr;
  //creates streaming socket & upon success, returns socket file descriptor
  sock = socket(PF_INET, SOCK_STREAM, 0);
  int on = 1;
  //set socket option to  allow reuse of local addresses
  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
	//gives socket properties
  memset(&socket_addr, 0, sizeof socket_addr);
  socket_addr.sin_family = PF_INET;
  socket_addr.sin_port = htons(port);
  socket_addr.sin_addr.s_addr = htonl(INADDR_ANY);

  //binds socket to socket address, will only 
  if (bind(sock, (struct sockaddr *)&socket_addr, sizeof socket_addr) < 0) {
    perror("couldn't bind");
    exit(1);
  }
  //makes socket passive, will be used to accept incoming connection requests
  listen(sock, 10);
	
  while (1) {
    // accept extracts the first connection on the queue of pending connections
    int acceptsock = accept(sock, NULL, NULL);
    char *input = (char *)malloc(1024*sizeof (char));
    //receives message from sockets
    recv(acceptsock, input, 1024, 0);
    int is_html = 0;
    char *contents = handle(input,&is_html);
    free(input);

    //how to respond to the user, depending on the file contents
    //in our stack smash we should not get here bc we should have overwritten the return address
    if (contents != NULL) {
      send(acceptsock, resp_ok, strlen(resp_ok), 0);
      if (is_html) {
	send(acceptsock, content_html, strlen(content_html), 0);
      } else {
	send(acceptsock, content_text, strlen(content_text), 0);
      }
      send(acceptsock, contents, strlen(contents), 0);
      send(acceptsock, "\n", 1, 0);
      free(contents);
    } else {
      send(acceptsock, resp_bad, strlen(resp_bad), 0);
    }    
    close(acceptsock);
  }

  return 0;
}

char *handle(char *request, int *is_html) {
  char filename[100];
  printf("addr+140: %x\n",((unsigned int)(&filename))+140);
  //printf("filename+140: %u\n", (filename+140));
  memset(filename, '\0', 100*sizeof (char));
  //checks if request starts with "Get /"
  if (strstr(request, prefix) == request) {
    char *start = request + strlen(prefix);
    char *end = strstr(start, " HTTP");
    if (end == NULL) {
      printf("Unsupported command.\n");
      return NULL;
    }
    //isolates length of the unique request string we put in
    int len = (int) (end - start);
    //checks to make sure our string is shorter than 100bytes... this is where we trick it!!!
    if (check_filename_length(len)) {
      //copies attack string to where file name is on the stack
      strncpy(filename, start, len);
    } else {
      return NULL;
    }  
  } else {
    printf("Unsupported command.\n");
    return NULL;
  }

  //makes sure filename doesn't have a data path confusion issue
  if (strstr(filename,"/") != NULL) {
    printf("%p\n", __builtin_return_address(0));
    printf("%x\n", *(unsigned int*)__builtin_return_address(0));
    return NULL;
  }
  
  //if no filename passed in, assume that they wanted index.html
  if (!strcmp(filename, "")) {
    strcpy(filename,"index.html");
  }

  //indicates that an html file was requested
  if (strstr(filename,".html") != NULL) {
    *is_html = 1;
  }


  char *path = (char *)malloc((strlen(www_path) + strlen(filename))*sizeof (char));
  strcpy(path, www_path);
  strcpy(path + strlen(www_path), filename);

  struct stat file_status;
  if (stat(path, &file_status) != 0) {
    //i think we're gonna enter this case bc our request isn't gonna be a real file
    return NULL;
  }
  char *contents = (char *)malloc((file_status.st_size+1)*sizeof (char));
  memset(contents, '\0', file_status.st_size+1);
	
  //open requested file
  FILE *readfile = fopen(path, "r");
  int i;
  //read file contents
  for (i=0;i<file_status.st_size;i++) {
    contents[i] = fgetc(readfile);
  }
  fclose(readfile);

  //return file contents
  return contents;
}

int check_filename_length(byte len) {
  if (len < 100) {
    return 1;
  }
  return 0;
}

//handles user sending signal interrupt
void sigint(int arg) {
  if (sock) {
    close(sock);
  }
  
  printf("Shutting down peacefully.\n");
  exit(0);
}

//handles segfault
void sigsegv(int arg) {
  if (sock) {
    close(sock);
  }
  
  printf("Segmentation fault.  Shutting down peacefully, then rebooting.\n");

  sleep(2);
  char *argv[3];
  /* change this to the path of your own webserver executable */
  argv[0] = "./webserver";
  argv[1] = port_arg;
  argv[2] = NULL;
  execv(argv[0], argv);
  free(port_arg);
  exit(1);
}
