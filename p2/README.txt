Names: Anna Sun, Kayla Schulz, Lisa Sapozhnikov
Time Spent: 25 hours
Additional Collaborators: None

Explanation of Method of Attack: As recommended in the write-up, after locating the
vulnerabilities in the webserver.c script, we first attacked the local server. We 
utilized the shellcode given on Piazza by Rubens, frontloaded with a large number of
nops to create our nop sled. This also overwrote variables stored on the stack.
We then used gdb to locate a memory address to attack. We then hacked into the local 
server. The remote server posed a new set of issues because we could not use gdb to locate
a memory address. Instead, we needed to try different addresses until we hit one that would
allow us access to the server. In order to locate this address, Anna wrote a bash script to loop
through potential addresses (0xbffff__01 where the underscores represent the numbers we looped through).
We ran through this script until the server hung, at which point we had found our address. In a separate terminal,
we then modified files on the remote server.

Review of Project: Overall, this was an interesting project utilizing techniques different from what
we have done at Duke thus far. I think the most frustrating part for us was spending a few hours with the
buggy shellcode and not understanding what our problem was. Once the updated shellcode was posted,
the project seemed to go by quicker. Additionally, as you would expect, as the days went by,
many more helpful hints and tips were posted to Piazza. Other students seemed to run into a lot of the
same issues we did, so it made the project much more efficient when we were able to go through Piazza posts
and understand where potential errors might occur.