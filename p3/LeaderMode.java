package edu.duke.raft;
import java.util.Timer;
public class LeaderMode extends RaftMode {
	// timer IDs
	protected final static int HEARTBEAT_TIMER = 1;
	// TODO: initialize nextIndex and matchIndex
	protected int[] mNextIndex;
	protected int[] mMatchIndex;
	Timer t;
	
	public void go () {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm();
			RaftResponses.clearAppendResponses(term);
			RaftResponses.clearVotes(term);
			RaftResponses.setTerm(term);
			System.out.println ("S" + 
					mID + 
					"." + 
					term + 
					": switched to leader mode.");
			
			int numServers = mConfig.getNumServers();
			mNextIndex = new int[numServers+1];
			mMatchIndex = new int[numServers+1];
			for (int serverID=1; serverID<=numServers; serverID++) {
				mNextIndex[serverID] = mLog.getLastIndex();
				mMatchIndex[serverID] = mNextIndex[serverID] - 1;
			}
			/* TODO: make remoteRPC calls whenever you get a new client request
			 * Log repair - start each follower's nextIndex at what my current log entry is.
			 * Decrement if it returns a lower term.
			 * What if it gets a return saying that there's another leader?
			 */ 
			t = scheduleTimer(HEARTBEAT_INTERVAL, HEARTBEAT_TIMER);
			
		}
	}
	// @param candidate’s term
	// @param candidate requesting vote
	// @param index of candidate’s last log entry
	// @param term of candidate’s last log entry
	// @return 0, if server votes for candidate; otherwise, server's
	// current term
	public int requestVote (int candidateTerm,
			int candidateID,
			int lastLogIndex,
			int lastLogTerm) {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm ();
			if (candidateTerm <= term){
				return term;
			}
			else{
				t.cancel();
				mConfig.setCurrentTerm(candidateTerm, 0);
				RaftServerImpl.setMode(new FollowerMode());
				return term;
			}
		}
	}
	// @param leader’s term
	// @param current leader
	// @param index of log entry before entries to append
	// @param term of log entry before entries to append
	// @param entries to append (in order of 0 to append.length-1)
	// @param index of highest committed entry
	// @return 0, if server appended entries; otherwise, server's
	// current term
	public int appendEntries (int leaderTerm,
			int leaderID,
			int prevLogIndex,
			int prevLogTerm,
			Entry[] entries,
			int leaderCommit) {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm ();
			if (leaderTerm > term) {
				t.cancel();
				mConfig.setCurrentTerm(leaderTerm, 0);
				RaftServerImpl.setMode(new FollowerMode());
			} 
			int result = term;
			return result;
		}
	}
	// @param id of the timer that timed out
	public void handleTimeout (int timerID) {
		synchronized (mLock) {
			if (timerID == HEARTBEAT_TIMER) {
				for (int serverID=1; serverID<=mConfig.getNumServers(); serverID++) {
					if (serverID == mID){
						continue;
					}
					remoteAppendEntries(serverID, 
							mConfig.getCurrentTerm(),
							mID,
							mMatchIndex[serverID-1],
							//mLog.getEntry(mMatchIndex[serverID]).term,
							-1,
							null,
							mCommitIndex);
					int response = RaftResponses.getAppendResponses(mConfig.getCurrentTerm())[serverID];
					//System.out.println("mID: " + mID + " term: " + mConfig.getCurrentTerm() + " response: " + response);
					if ((Integer) response == null) {
						throw new IllegalArgumentException();
					}
					if (response == 0) {
						// success, keep going
						continue;
					} else {
						if (response > mConfig.getCurrentTerm()) {
							t.cancel();
							mConfig.setCurrentTerm(response, 0);
							RaftServerImpl.setMode(new FollowerMode());
						} // TODO: handle ELSE case for rejected for repair.
					}
				}
				t.cancel();
				t = scheduleTimer(HEARTBEAT_INTERVAL, HEARTBEAT_TIMER);
			}
		}
	}
}