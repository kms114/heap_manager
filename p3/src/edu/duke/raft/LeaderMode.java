package edu.duke.raft;
import java.util.Arrays;
import java.util.Timer;
public class LeaderMode extends RaftMode {
	// timer IDs
	protected final static int HEARTBEAT_TIMER = 1;
	protected final static int REPAIR_TIMER = 5;
	protected final static int REPAIR_INTERVAL = HEARTBEAT_INTERVAL/8;
	// TODO: initialize nextIndex and matchIndex
	protected int[] mNextIndex;
	protected int[] mMatchIndex;
	Timer t;
	Timer rt;
	
	public void go () {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm();
			RaftResponses.clearAppendResponses(term);
			RaftResponses.clearVotes(term);
			RaftResponses.setTerm(term);
			System.out.println ("S" + 
					mID + 
					"." + 
					term + 
					": switched to leader mode.");
			
			int numServers = mConfig.getNumServers();
			mNextIndex = new int[numServers+1];
			mMatchIndex = new int[numServers+1];
			for (int serverID=1; serverID<=numServers; serverID++) {
				mNextIndex[serverID] = mLog.getLastIndex() + 1;
				mMatchIndex[serverID] = 0;
			}
			/* TODO: make remoteRPC calls whenever you get a new client request
			 * Log repair - start each follower's nextIndex at what my current log entry is.
			 * Decrement if it returns a lower term.
			 * What if it gets a return saying that there's another leader?
			 */ 
			t = scheduleTimer(HEARTBEAT_INTERVAL, HEARTBEAT_TIMER);
			rt = scheduleTimer(REPAIR_INTERVAL, REPAIR_TIMER);

		}
	}
	// @param candidate’s term
	// @param candidate requesting vote
	// @param index of candidate’s last log entry
	// @param term of candidate’s last log entry
	// @return 0, if server votes for candidate; otherwise, server's
	// current term
	public int requestVote (int candidateTerm,
			int candidateID,
			int lastLogIndex,
			int lastLogTerm) {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm ();
			if (candidateTerm <= term){
				return term;
			}
			else{
				t.cancel();
				rt.cancel();
				mConfig.setCurrentTerm(candidateTerm, 0);
				//System.out.println("candidateTerm: " + candidateTerm);
				//System.out.println("leader " + mID + "." + term + " to follower " + mID + "." + candidateTerm);
				RaftServerImpl.setMode(new FollowerMode());
				return term;
			}
		}
	}
	// @param leader’s term
	// @param current leader
	// @param index of log entry before entries to append
	// @param term of log entry before entries to append
	// @param entries to append (in order of 0 to append.length-1)
	// @param index of highest committed entry
	// @return 0, if server appended entries; otherwise, server's
	// current term
	public int appendEntries (int leaderTerm,
			int leaderID,
			int prevLogIndex,
			int prevLogTerm,
			Entry[] entries,
			int leaderCommit) {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm ();
			if (leaderTerm > term) {
				t.cancel();
				rt.cancel();
				mConfig.setCurrentTerm(leaderTerm, 0);
				//System.out.println("leaderTerm: " + leaderTerm);
				//System.out.println("leader " + mID + "." + term + "-> follower " + mID + "." + leaderTerm );
				RaftServerImpl.setMode(new FollowerMode());
			} 
			return term;
		}
	}
	// @param id of the timer that timed out
	public void handleTimeout (int timerID) {
		if (timerID == HEARTBEAT_TIMER) {
			synchronized (mLock) {
				int term = mConfig.getCurrentTerm();
				for (int serverID=1; serverID<=mConfig.getNumServers(); serverID++) {
					if (serverID == mID){
						continue;
					}
					int response = RaftResponses.getAppendResponses(mConfig.getCurrentTerm())[serverID];
					//System.out.println("mID: " + mID + " term: " + mConfig.getCurrentTerm() + " response: " + response);
					if ((Integer) response == null) {
						throw new IllegalArgumentException();
					}
					if (response == 0 || response == -1) {
						continue;
					} else {
						if (response > term) {
							t.cancel();
							rt.cancel();
							mConfig.setCurrentTerm(response, 0);
							// TODO: take out this print
							//System.out.println("leader " + mID + "got response > its term " + term);
							//System.out.println("response: " + response);
							RaftServerImpl.setMode(new FollowerMode());
							return;
						} 
						//						else {
						//							mNextIndex[serverID] -= 1;
						//						}
					}
				}
				//RaftResponses.clearAppendResponses(mConfig.getCurrentTerm());
				for (int serverID=1; serverID<=mConfig.getNumServers(); serverID++) {
					if (serverID == mID) {
						continue;
					}
					//System.out.println("leader " + mID + "." + term + " sending heartbeat to " + serverID);
					remoteAppendEntries(serverID, 
							mConfig.getCurrentTerm(),
							mID,
							mNextIndex[serverID] - 1, // NOTE: updated this.
							mLog.getEntry(mNextIndex[serverID] - 1).term,
							null,
							mCommitIndex);
					
				}
				t.cancel();
				t = scheduleTimer(HEARTBEAT_INTERVAL, HEARTBEAT_TIMER);
			}
		}
		else if (timerID == REPAIR_TIMER) {
			synchronized (mLock) {
				rt.cancel();
				boolean stillUpdating = false;
				int term = mConfig.getCurrentTerm();
				for (int serverID=1; serverID<=mConfig.getNumServers(); serverID++) {
					if (serverID == mID){
						continue;
					}
					int response = RaftResponses.getAppendResponses(mConfig.getCurrentTerm())[serverID];
					//System.out.println("mID: " + mID + " term: " + mConfig.getCurrentTerm() + " response: " + response);
					if ((Integer) response == null) {
						throw new IllegalArgumentException();
					}
					if (response == -1){
						stillUpdating = true;
						//System.out.println("Response is -1, myID: " + serverID);
						continue;
					}
					if (response == 0) {
						// success, logs match
						mMatchIndex[serverID] = mLog.getLastIndex();
						mNextIndex[serverID] = mMatchIndex[serverID]+1;
					} else {
						if (response > term) {
							t.cancel();
							rt.cancel();
							mConfig.setCurrentTerm(response, 0);
							//System.out.println("leader " + mID + "." + term + " changing to follower");
							//System.out.println("serverID: " + serverID + " response: " + response);
							RaftServerImpl.setMode(new FollowerMode());
							return;
						} 
						else {
							// logs don't match
							stillUpdating = true;
							mNextIndex[serverID] -= 1;
							if (mNextIndex[serverID] == 0){
								//System.out.println("WTF NEXT ID is 0 for sever: " + serverID);
							}
						}
					}
				}
				//RaftResponses.clearAppendResponses(mConfig.getCurrentTerm());

				if (stillUpdating){
					for (int serverID=1; serverID<=mConfig.getNumServers(); serverID++) {
						if (serverID == mID){
							continue;
						}
						// TODO: what if mNextIndex[serverID]-1 == -1 or > mLog.getLastIndex()?
						// mNextIndex[serverID] initialized to mLog.getLastIndex + 1
						// if lastIndex = 0, mNextIndex[serverID] initialized to 1,
						// shouldn't throw error
						// decrement mNextIndex every time it fails.

						// if it starts at 
						if (mNextIndex[serverID]-1 == -1 || mNextIndex[serverID]-1 > mLog.getLastIndex()) {
							//System.out.println("illegal argument, mNextIndex[serverID]-1: " + (mNextIndex[serverID]-1));
							throw new IllegalArgumentException();
						}

						//TODO: why getlastindex+1?
						if (mLog.getLastIndex()+1 >= mNextIndex[serverID]) {
							// TODO: double check entries
							Entry[] entries = new Entry[mLog.getLastIndex()+1-mNextIndex[serverID]];
							for (int i=mNextIndex[serverID]; i<mLog.getLastIndex()+1; i++) {
								//System.out.println("mNextIndex[serverID]: " + mNextIndex[serverID] + " mLog.getLastIndex()+1" + mLog.getLastIndex()+1);
								entries[i-mNextIndex[serverID]] = mLog.getEntry(i);
							}
							//System.out.println("Leader's entries to send: " + Arrays.toString(entries) + " to server: " + serverID);
							
							//TODO: THIS ONE FIXES
							//System.out.println(mID + "." + term + " sending repair to " + serverID);
							remoteAppendEntries(serverID, 
									mConfig.getCurrentTerm(),
									mID,
									mNextIndex[serverID] - 1, // NOTE: updated this.
									mLog.getEntry(mNextIndex[serverID] - 1).term,
									entries,
									mCommitIndex);
						}
					}
					rt = scheduleTimer(REPAIR_INTERVAL, REPAIR_TIMER);
				}


			}
			// TODO: update mCommitIndex to N if N>mCommitIndex,
			// majority of matchIndex[i] >= N, and log[N].term == currentTerm


		}
	}
}