package edu.duke.raft;

import java.util.Random;
import java.util.Timer;

public class CandidateMode extends RaftMode {
	boolean electionOverride = false;
	Timer timerElection;
	Timer timerCheckvotes;	
	int timeoutLength;
	protected final static int ELECTION_TIMEOUT = 3;
	protected final static int CHECKVOTES_TIMEOUT = 4;
	public void go () {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm()+1;    
			RaftResponses.clearAppendResponses(term);
			RaftResponses.clearVotes(term);
			mConfig.setCurrentTerm(term, mID);
			RaftResponses.setTerm(term);
			System.out.println ("S" + 
					mID + 
					"." + 
					term + 
					": switched to candidate mode.");
			if (electionOverride){
				timeoutLength = mConfig.getTimeoutOverride();
				//System.out.println("timeoutLengthCandidate : " + timeoutLength);
			}
			else{
				Random rand = new Random();
				timeoutLength = rand.nextInt(ELECTION_TIMEOUT_MAX-ELECTION_TIMEOUT_MIN+1) + ELECTION_TIMEOUT_MIN;
				//System.out.println("timeoutLengthCandidate : " + timeoutLength);
			}
			timerElection = scheduleTimer(timeoutLength, ELECTION_TIMEOUT);
			timerCheckvotes = scheduleTimer(timeoutLength/16, CHECKVOTES_TIMEOUT);
			//System.out.println("schedule timers");
			for (int i = 1; i <= mConfig.getNumServers(); i++){
				if (i == mID){
					RaftResponses.setVote(mID, 0, term);
				}
				else{
					remoteRequestVote(i, term, mID, mLog.getLastIndex(), mLog.getLastTerm());
				}
			}
		}
	}

	// @param candidate’s term
	// @param candidate requesting vote
	// @param index of candidate’s last log entry
	// @param term of candidate’s last log entry
	// @return 0, if server votes for candidate; otherwise, server's
	// current term 
	public int requestVote (int candidateTerm,
			int candidateID,
			int lastLogIndex,
			int lastLogTerm) {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm ();
			if (candidateTerm <= term){
				return term;
			}
			else if (mLog.getLastTerm() > lastLogTerm) {
				return term;
			}
			else if (mLog.getLastTerm() == lastLogTerm && mLog.getLastIndex() > lastLogIndex) {
				return term;
			}
			else{
				// Switch to follower if your term is less than incoming candidate term
				mConfig.setCurrentTerm(candidateTerm, 0);
				timerElection.cancel();
				timerCheckvotes.cancel();
				//System.out.println("cancel timers");
				RaftServerImpl.setMode(new FollowerMode());
				return term;
			}
			
		}
	}


	// @param leader’s term
	// @param current leader
	// @param index of log entry before entries to append
	// @param term of log entry before entries to append
	// @param entries to append (in order of 0 to append.length-1)
	// @param index of highest committed entry
	// @return 0, if server appended entries; otherwise, server's
	// current term
	public int appendEntries (int leaderTerm,
			int leaderID,
			int prevLogIndex,
			int prevLogTerm,
			Entry[] entries,
			int leaderCommit) {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm ();
			if (leaderTerm < term){
				//outdated leader
				return term;
			}
			else{
				//outdated candidate
				timerElection.cancel();
				timerCheckvotes.cancel();
				//System.out.println("cancel timers");
				mConfig.setCurrentTerm(leaderTerm, 0);
				RaftServerImpl.setMode(new FollowerMode());
				return term;

			}
		}
	}

	// @param id of the timer that timed out
	public void handleTimeout (int timerID) {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm();
			if (timerID == ELECTION_TIMEOUT){
				//System.out.println("election timeout & restart for server: " + mID);
				timerElection.cancel();
				timerCheckvotes.cancel();
				//System.out.println("cancel timers");
				this.go();
			}
			if (timerID == CHECKVOTES_TIMEOUT){
				//System.out.println("checkvotes timeout");
				int numVotes = 0;
				int maxTerm = term;
				for (int i = 1; i <= mConfig.getNumServers(); i++){
					//System.out.println("arrayLength :" + RaftResponses.getVotes(term).length);
					//System.out.println("index : " + i + " vote: "+ RaftResponses.getVotes(term)[i]);
					if (RaftResponses.getVotes(term)[i] > maxTerm){
						// TODO: shouldn't you switch to the one with the highest term?
						// maybe it doesn't matter b/c you'll immediately set new term upon receiving heartbeat
						maxTerm = RaftResponses.getVotes(term)[i];
					}
				}
				if (maxTerm > term){
					timerElection.cancel();
					timerCheckvotes.cancel();
					//System.out.println("cancel timers");
					mConfig.setCurrentTerm(maxTerm, 0);
					//System.out.println("myID: " + mID + " switching to follower mode bc higher term: " + maxTerm);
					RaftServerImpl.setMode(new FollowerMode());
					return;
				}
				for (int i = 1; i <= mConfig.getNumServers(); i++){
					if (RaftResponses.getVotes(term)[i] == 0){
						numVotes++;
						if (numVotes > mConfig.getNumServers()/2){
							// got majority of votes
							timerElection.cancel();
							timerCheckvotes.cancel();
							//System.out.println("cancel timers");
							RaftServerImpl.setMode(new LeaderMode());
							return;
						}
					}
				}
				timerCheckvotes.cancel();
				//System.out.println("cancel only checkvotes timer");
				timerCheckvotes = scheduleTimer(timeoutLength/16, CHECKVOTES_TIMEOUT);
				//System.out.println("schedule only checkvotes timer");
			}
		}
	}
}
