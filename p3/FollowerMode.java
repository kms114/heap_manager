package edu.duke.raft;
import java.util.Random;
import java.util.Timer;

public class FollowerMode extends RaftMode {
	boolean electionOverride = false;
	
	protected final static int LEADER_TIMEOUT = 2;
	Timer timer;
	int timeoutLength;
	public void go () {
		synchronized (mLock) {
			int term = mConfig.getCurrentTerm();
			//System.out.println("INIT mConfig.getVotedFor(): " + mConfig.getVotedFor());
			RaftResponses.clearAppendResponses(term);
			RaftResponses.clearVotes(term);
			RaftResponses.setTerm(term);
			System.out.println ("S" + 
					mID + 
					"." + 
					term + 
					": switched to follower mode.");
			if (electionOverride){
				timeoutLength = mConfig.getTimeoutOverride();
				//System.out.println("timeoutLengthFollower : " + timeoutLength);
			}
			else{
				Random rand = new Random();
				timeoutLength = rand.nextInt(ELECTION_TIMEOUT_MAX-ELECTION_TIMEOUT_MIN+1) + ELECTION_TIMEOUT_MIN;
				//System.out.println("timeoutLengthFollower : " + timeoutLength);
			}
			timer = scheduleTimer(timeoutLength, LEADER_TIMEOUT);
		}
	}

	// @param candidate’s term
	// @param candidate requesting vote
	// @param index of candidate’s last log entry
	// @param term of candidate’s last log entry
	// @return 0, if server votes for candidate; otherwise, server's
	// current term
	public int requestVote (int candidateTerm,
			int candidateID,
			int lastLogIndex,
			int lastLogTerm) {
		synchronized (mLock) {
			//System.out.println("follower is voting ID: " + mID + " candidate ID: " + candidateID);
			int term = mConfig.getCurrentTerm ();
			if (candidateTerm <= term){
				return term;
			}
			// TODO: compare log indices
			else{
				//System.out.println("myID :" + mID + " voted for: " + candidateID);
				timer.cancel();
				timer = scheduleTimer(timeoutLength, LEADER_TIMEOUT);
				mConfig.setCurrentTerm(candidateTerm, candidateID);
				return 0;
			}
		}
	}


	// @param leader’s term
	// @param current leader
	// @param index of log entry before entries to append
	// @param term of log entry before entries to append
	// @param entries to append (in order of 0 to append.length-1)
	// @param index of highest committed entry
	// @return 0, if server appended entries; otherwise, server's
	// current term
	public int appendEntries (int leaderTerm,
			int leaderID,
			int prevLogIndex,
			int prevLogTerm,
			Entry[] entries,
			int leaderCommit) {
		synchronized (mLock) {
			//System.out.println("follower append entries- myID: " + mID + " leaderID: " + leaderID);
			int term = mConfig.getCurrentTerm ();
			if (leaderTerm < term){
				//System.out.println("stale leader appendEntry attempt- myID: " + mID + " leaderID: " + leaderID);
				return term;
			}
			if (leaderTerm > term){
				mConfig.setCurrentTerm(leaderTerm, 0);
			}
			timer.cancel();
			timer = scheduleTimer(timeoutLength, LEADER_TIMEOUT);
			if (entries == null){
				//System.out.println("heartbeat- myID: " + mID + " leaderID: " + leaderID);
				return 0;
			}
			return 0;
		}
	}  

	// @param id of the timer that timed out
	public void handleTimeout (int timerID) {
		synchronized (mLock) {
			timer.cancel();
			RaftServerImpl.setMode(new CandidateMode());
		}
	}
}

